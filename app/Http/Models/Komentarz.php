<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

use App\Http\Models\Alkohol;

use Auth;

class Komentarz extends Model{


	protected $table = 'komentarz';
    protected $fillable = array('komentarz_id','alkohol_id','recenzja_id', 'uuid', 'text');
	//protected $hidden = ['password', 'activate_token'];
	public  $timestamps = false;
	protected $primaryKey = 'komentarz_id';
	
		public function uzytkownik()
		{
			return $this->hasOne('App\Http\Models\Uzytkownik', 'uuid','uuid');
		}
		
	
	
	/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */
	
	
		public function showCommentsByUser($id) //zwraca array
		{
			return Komentarz::where('uuid',$id)->get();
			
		}
		
		
		public function deleteKomMy($id) //nic nie zwraca
		{
			Komentarz::where('komentarz_id',$id)->where('uuid',Auth::id())->delete();
			
		}
		
		public function deleteKom($id)// nic nie zwraca
		{	
			
			$komentarz = Komentarz::find($id);
			$komentarz->delete();
			
		}
	
		public function addKomToRec() //nic nie zwraca
		{
			try
			{
				$rec = Recenzja::findOrFail(Input::get('komid'));
				$kom = new Komentarz();
				$kom->recenzja_id = Input::get('komid');
				$kom->uuid = Auth::id();
				$kom->text = Input::get('komVal');
				$kom->save();				
			}
			catch(\Exception $e)
			{
				throw new \Exception($e->getMessage());
			}
			
			
		}
	
		public function addKomToAlko() //nic nie zwraca
		{
			try
			{
				$alko = Alkohol::findOrFail(Input::get('komid'));
				$kom = new Komentarz();
				$kom->alkohol_id = Input::get('komid');
				$kom->uuid = Auth::id();
				$kom->text = Input::get('komVal');
				$kom->save();				
			}
			catch(\Exception $e)
			{
				throw new \Exception($e->getMessage());
			}			
		}
		
		
		
		public function komLast($many) //zwraca array
		{
			$kom = Komentarz::with('uzytkownik')->orderBy('komentarz_id','DESC')->take($many)->get();
			return $kom;
		}
		
		public function komListRec($id) //zwraca array
		{
			$kom = Komentarz::with('uzytkownik')->where('recenzja_id',$id)->get();
			return $kom;
		}

		public function komListAlko($id) //zwraca array
		{
			$kom = Komentarz::with('uzytkownik')->where('alkohol_id',$id)->get();
			return $kom;
		}
	
		
		
}
