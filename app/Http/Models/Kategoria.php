<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;


class Kategoria extends Model{


	protected $table = 'kategoria';
    protected $fillable = array('kategoria_id','nazwa');
	//protected $hidden = ['password', 'activate_token'];
	public  $timestamps = false;
	protected $primaryKey = 'kategoria_id';
		
		//to nizej to jakis eager loading czyli lepszy
		public function alkoholCount()
		{
		  return $this->hasOne('App\Http\Models\Alkohol', 'kategoria_id', 'kategoria_id')
			->selectRaw('kategoria_id, count(*) as aggregate')
			->groupBy('kategoria_id');
		}
		 
		public function getArticlesCountAttribute()
		{
		  // if relation is not loaded already, let's do it first
		  if ( ! array_key_exists('alkoholCount', $this->relations)) 
			$this->load('alkoholCount');
		 
		  $related = $this->getRelation('alkoholCount');
		 
		  // then return the count directly
		  return ($related) ? (int) $related->aggregate : 0;
		}	
		
	


	/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */
	
		
	public function categorySelectArray()//zwraca array
	{
		$category = Kategoria::all();
		$array = [];
		if(!empty($category))
		{
			foreach($category as $ct)
			{
				$array[$ct->kategoria_id] = $ct->nazwa;
			}
		}
		return $array;
		
	}
		
		public function categoryName($id)//zwraca String
		{
			$category = Kategoria::find($id);
			return $category->nazwa;
		}
	
	public function catList()//zwraca array
	{
			$category = Kategoria::with('alkoholCount')->get();
			return $category;
		
	}
	
	
	
	public function editCategory()//nic nie zwraca
	{
		try
		{
			$kat = Kategoria::findOrFail(Input::get('catId'));
			$kat->nazwa = Input::get('catName');
			$kat->save();
		}
		catch(\Exception $e)
		{
			throw new \Exception(Lang::get('msg.catNotExist'));
		}
		
	}
		
	public function addNewCat()//nic nie zwraca
	{
		$exist = Kategoria::where('nazwa',Input::get('catName'))->count();
		if(!$exist){
			$kat = new Kategoria();
			$kat->nazwa = Input::get('catName');
			$kat->save();
		}else throw new \Exception(Lang::get('msg.catExist'));
		
	}
	
	
	public function delCat($id){//nic nie zwraca
		$kat = Kategoria::find($id);
		if(count($kat)){
			$kat->delete();
		}
	}
		
		
			
	
		
		
}
