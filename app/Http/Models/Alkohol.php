<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

use App\Http\Models\Glosy;


use Image;
use Auth;

use File;

class Alkohol extends Model{


	protected $table = 'alkohol';
    protected $fillable = array('alkohol_id','nazwa','img', 'text', 'kategoria_id', 'glosyLiczba', 'glosySuma');
	//protected $hidden = ['password', 'activate_token'];
	public  $timestamps = false;
	protected $primaryKey = 'alkohol_id';
	
		public function kategoria()
		{
			return $this->hasOne('App\Http\Models\Kategoria', 'kategoria_id','kategoria_id');
		}
		
		public function recenzja()
		{
			return $this->hasOne('App\Http\Models\Recenzja', 'alkohol_id','alkohol_id');
		}
		
		
		
		
		
		/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */
		
		public function voteAlko()//NIC NIE ZWRACA
		{
			if(Input::has('komid') && is_numeric(Input::get('voteVal'))){
				try
				{
					$alk = Alkohol::findOrFail(Input::get('komid'));
					$glosy = new Glosy();
					$glosy->voteOn(Input::get('komid'),Auth::id(),Input::get('voteVal'));
					$nowe = $glosy->newSumCount(Input::get('komid'));
					$alk->glosyLiczba = $nowe['liczba'];
					$alk->glosySuma = $nowe['suma'];
					$alk->save();
				}
				catch(\Exception $e)
				{
					throw new \Exception($e->getMessage());
				}
			}else throw new \Exception("Nie hakuj!");
		}
		
		public function getValueAVG()//ZWRACA DOUBLE albo float zalezy jaki wynik wyjdzie xD nawet int zwraca :D ale to spisz float
		{
			$alko = Alkohol::find(Input::get('komid'));
			if($alko->glosySuma > 0)
				return $alko->glosySuma / $alko->glosyLiczba;
			else
				return 0;
			
		}
		
		public function alkoholSelectList()//zwraca array
		{
			$alk = Alkohol::all();
			$array = [];
			if(!empty($alk))
			{
				foreach($alk as $at){
					$array[$at->alkohol_id] = $at->nazwa;
				}
			}
			return $array;
		}
		
		
		public function editAlkohol()//nic nie zwraca
		{
			try
			{
				$alk = Alkohol::findOrFail(Input::get('alkidE'));
				$alk->nazwa = Input::get('alkNameE');
				$alk->text = Input::get('alkDescE');
				$alk->kategoria_id = Input::get('alkKategoriaE');
				if(Input::hasFile('image'))
				{	
					if(file_exists($alk->img)){
						unlink($alk->img);
					}
					$destinationPath = 'resources/upload'; // upload path
					$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
					$fileName = str_random(25).'.'.$extension; // renameing image
					Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
					$image = Image::make($destinationPath.'/'.$fileName);
					$image->resize(1050, null, function ($constraint) {
						$constraint->aspectRatio();
					});
					$image->save();
					$alk->img = $destinationPath.'/'.$fileName;
				}
				$alk->save();
			}
			catch(\Exception $e)
			{
				throw new \Exception('Nie ma takiego alkoholu');
			}
		}
		
		public function deleteAlkohol($id)//nic nie zwraca
		{
			try
			{
				$alk = Alkohol::findOrFail($id);
				if(file_exists($alk->img))
				{
					unlink($alk->img);
				}
				$alk->delete();
			}
			catch(\Exception $e)
			{
				throw new \Exception("Nie ma takiego Alkoholu");
			}
		}
		
		
		public function addNewAlkohol()//nic nie zwraca
		{
			if (Input::hasFile('image'))
			{
				$alk = new Alkohol();
				$alk->nazwa = Input::get('alkName');
				$alk->text = Input::get('alkDesc');
				$alk->kategoria_id = Input::get('alkKategoria');
				$destinationPath = 'resources/upload'; // upload path
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				$fileName = str_random(25).'.'.$extension; // renameing image
				Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
				$image = Image::make($destinationPath.'/'.$fileName);
				$image->resize(720, null, function ($constraint) {
					$constraint->aspectRatio();
				});
				$image->save();
				$alk->img = $destinationPath.'/'.$fileName;
				$alk->save();
			}else throw new \Exception("Musisz dodać obrazek");
		}

		
		
		public function alkoInfo($id)//zwraca objekt
		{
			return Alkohol::with('kategoria')->with('recenzja')->where('alkohol_id',$id)->get();
		}
		
		public function alkoholListFromCategory($id)//zwraca array
		{
			$alkohol = Alkohol::with('kategoria')->with('recenzja')->where('kategoria_id',$id)->get();
			return $alkohol;
		}
		
		public function alkoholList()//zwraca array
		{
			$alkohol = Alkohol::with('kategoria')->with('recenzja')->get();
			return $alkohol;
			
		}
	
		
		
}
