<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

use App\Http\Models\Komentarz;

use Hash;
use Mail;
use Auth;

class Uzytkownik extends Model implements AuthenticatableContract, CanResetPasswordContract{

use Authenticatable, CanResetPassword;

	protected $table = 'uzytkownik';
    protected $fillable = array('uuid','username', 'password', 'email', 'activate_token', 'active', 'admin', 'remember_token');
	//protected $hidden = ['password', 'activate_token'];
	public  $timestamps = false;
	protected $primaryKey = 'uuid';
		
		
	
	/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */	
		
		
	public function loginUser(){ //nic nie zwraca
		$exist = Uzytkownik::where('username', Input::get('username'))->count();
		if($exist){
			if(!Auth::attempt(['username'=>Input::get('username'), 'password'=>Input::get('password'), 'active'=>1])){
				$active = Uzytkownik::where('username', Input::get('username'))->get();
				if($active[0]->active){
					throw new \Exception('Nieprawidłowe hasło');
				}else throw new \Exception('Konto nieaktywne');
			}
		}else throw new \Exception('Nie ma takiego użytkownika');
		
		
	}
		
	public function activateUser($code){ //zwraca string
		$exist = Uzytkownik::where('activate_token', $code)->get();
		if(count($exist)){
			$user = Uzytkownik::find($exist[0]->uuid);
			$user->active = 1;
			$user->activate_token = 'activated';
			$user->save();
			return "Konto zostało aktywowane";
		}else throw new \Exception("Nie ma takiego kodu lub został już aktywowany!");
		
	}
		
	public function addNewUser(){ //zwraca string
		if(Input::get('password1') == Input::get('password2'))
		{
			$count = Uzytkownik::where('username', Input::get('username'))->count();
			if(!$count)
			{
				$count2 = Uzytkownik::where('email', Input::get('email'))->count();
				if(!$count2)
				{
					$user = new Uzytkownik();
					$user->username = Input::get('username');
					$user->password = Hash::make(Input::get('password1'));
					$user->email = Input::get('email');
					$registerToken = str_random(50);
					$user->activate_token = $registerToken;
					$mailTxt = Lang::get('msg.emailActivationTextLink', ['link' => url('activate', ["code"=>$registerToken])]);
					Mail::raw($mailTxt, function($message)
					{
						$message->subject(Lang::get('msg.emailActivateSubject'));
						$message->from('moczniak@gmail.com', Lang::get('msg.emailActivateFrom'));
						$message->to(Input::get('email'));
					});
					$user->save();
					return Lang::get('msg.CheckUrEmail');
				}else throw new \Exception(Lang::get('msg.emailAlreadyUser'));
			}else throw new \Exception(Lang::get('msg.userExist'));
		}else
		{
			throw new \Exception(Lang::get('msg.passwordNotMatch'));
		}
	}

	public function delUser($id)//nic nie zwraca
	{
		try
		{
			$user = Uzytkownik::findOrFail($id);
			Komentarz::where('uuid',$id)->delete();
			$user->delete();
			//$komentarz = new Komentarz();
			//$komentarz->deleteByUser($id);
		}
		catch(\Exception $e)
		{
			throw new \Exception("Nie ma takiego użytkownika");
		}
	}

	public function usersList() //zwraca array
	{
		$user = Uzytkownik::where('uuid','!=',Auth::id())->get();
		return $user;		
	}
	
		
		
}
