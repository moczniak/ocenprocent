<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

use App\Http\Models\Alkohol;

use Auth;

class Glosy extends Model{


	protected $table = 'glosy';
    protected $fillable = array("glosy_id",'uuid','alkohol_id','vote');
	public  $timestamps = false;
	protected $primaryKey = "glosy_id";
	
		
		
		
	/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */
		
		
	public function myVote($id){//zwraca int
		if(Auth::check())
		{
			$vote = Glosy::where('alkohol_id',$id)->where('uuid',Auth::id())->get();
			
			if(!$vote->isEmpty())
				return $vote[0]->vote;
			else 
				return 0;
			
		}
		return 0;
	}	
		
		
		
		
	public function voteOn($alkohol_id,$uuid,$vote)//nic nie zwraca
	{
		
		$exist = Glosy::where('alkohol_id',$alkohol_id)->where('uuid',$uuid)->count();
		
		if($exist)
		{
			Glosy::where('alkohol_id',$alkohol_id)->where('uuid',$uuid)->update(array('vote'=>$vote));			
		}
		else
		{
			$glosA = new Glosy();
			$glosA->uuid = $uuid;
			$glosA->alkohol_id = $alkohol_id;
			$glosA->vote = $vote;
			$glosA->save();
		}
		
		
	}
	
	public function newSumCount($alkohol_id)//zwraca array
	{
		$sum = Glosy::where('alkohol_id',$alkohol_id)->sum('vote');
		$count = Glosy::where('alkohol_id',$alkohol_id)->count();
		return ["suma"=>$sum, "liczba"=>$count];
	}
		
}
