<?php namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

use Image;

use File;

class Recenzja extends Model{


	protected $table = 'recenzja';
    protected $fillable = array('recenzja_id','alkohol_id','text');
	//protected $hidden = ['password', 'activate_token'];
	public  $timestamps = false;
	protected $primaryKey = 'recenzja_id';
	
		
		
		public function alkohol()
		{
			return $this->hasOne('App\Http\Models\Alkohol', 'alkohol_id','alkohol_id');
		}
		
		
		
		/* TE FUNKCJE PONIZEJ WPISZ DO DIAGRAMU KLAS */
		
		public function recInfo($id) //zwraca objekt
		{
			$rec = Recenzja::with('alkohol')->find($id);
			return $rec;
			
		}
		
		
		
		public function lastRecenzja($set){ //zwraca array
			$rec = Recenzja::with('alkohol')->take($set)->get();
			return $rec;
		}

		public function recenzjaList() //zwraca array
		{
			$rec = Recenzja::with('alkohol')->get();
			return $rec;
			
		}
		
		public function delRecenzja($id) //nic nie zwraca
		{
			try
			{
				$rec = Recenzja::findOrFail($id);
				$rec->delete();
			}
			catch(\Exception $e)
			{
				throw new \Exception('Nie ma takiej recenzji');
			}
			
		}
		
		public function editRecenzja() //nic nie zwraca
		{
			try
			{
				$rec = Recenzja::findOrFail(Input::get('recid'));
				$rec->text = Input::get('recTxt');
				$rec->save();
			}
			catch(\Exception $e)
			{
				throw new \Exception('Nie ma takiej recenzji');
			}
			
		}
		
		public function addRecenzja() //nic nie zwraca
		{
			$exist = Recenzja::where('alkohol_id',Input::get('alkid'))->count();
			if(!$exist){
				$rec = new Recenzja();
				$rec->alkohol_id = Input::get('alkid');
				$rec->text = Input::get('recTxt');
				$rec->save();
			}else throw new \Exception("Ten Alkohol ma już recenzje");
		}
	
		
		
}
