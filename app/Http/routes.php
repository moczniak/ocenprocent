<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(array('namespace' => 'Admin', 'middleware'=>'adminAuth', 'prefix'=>'admin'), function(){
	Route::get('panel', 'IndexAdminController@index');
	Route::get('category', 'IndexAdminController@categories');
	Route::get('alkohols','IndexAdminController@alkohols');
	Route::get('review', 'IndexAdminController@reviews');
	Route::get('users',  'IndexAdminController@users');
	Route::post('addCategory', 'IndexAdminController@addCategory');
	Route::get('catDel/{id}', 'IndexAdminController@delCategory')->where('id','[0-9]+');
	Route::post('editCategory', 'IndexAdminController@editCategory');
	Route::post('addAlkohol', 'IndexAdminController@addAlkohol');
	Route::get('delAlkohol/{id}', 'IndexAdminController@delAlkohol')->where('id','[0-9]+');
	
	Route::post('editAlkohol', 'IndexAdminController@editAlkohol');
	
	Route::post('addRecenzja', 'IndexAdminController@addRecenzja');
	Route::post('editRecenzja',  'IndexAdminController@editRecenzja');
	
	Route::get('delRecenzja/{id}',   'IndexAdminController@delRecenzja')->where('id','[0-9]+');
	Route::get('delUser/{id}',   'IndexAdminController@delUser')->where('id','[0-9]+');
	
	Route::get('showComments/{id}', 'IndexAdminController@showCommentsByUser')->where('id','[0-9]+');
	
	
	Route::get('delKom/{id}', 'IndexAdminController@delComment')->where('id','[0-9]+');
	
});


Route::group(array('namespace' => 'User', 'middleware'=>'userAuth', 'prefix'=>'user'), function(){
	
	Route::get('comments', 'IndexUserController@comments');
	
	
	Route::get('delKom/{id}', 'IndexUserController@delComment')->where('id','[0-9]+');
	
	
	Route::post('addKom', 'IndexUserController@addKom');
	Route::post('voteAlk',  'IndexUserController@voteAlko');
	Route::post('addKomRec', 'IndexUserController@addKomRec');
	
});

Route::group(array('namespace' => 'Guest'), function(){
	Route::get('/', 'IndexGuestController@index');
	Route::get('activate/{code}',  'IndexGuestController@activateAccount');
	
	Route::post('register',  'IndexGuestController@register');
	Route::post('login',  'IndexGuestController@login');
	
	Route::get('logout',   'IndexGuestController@logout');
	
	Route::get('showCategory/{id}', 'IndexGuestController@showCategory')->where('id','[0-9]+');
	
	Route::get('showAlk/{id}',  'IndexGuestController@showAlkohol')->where('id','[0-9]+');
	
	Route::get('showRec/{id}',  'IndexGuestController@showRecenzja')->where('id','[0-9]+');
	
	Route::get('showRecs',  'IndexGuestController@showRecenzjaList');
	
});