<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Kategoria;
use App\Http\Models\Alkohol;
use App\Http\Models\Recenzja;
use App\Http\Models\Uzytkownik;
use App\Http\Models\Komentarz;

use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use \App\Http\Requests\AddAlkoholRequest;
use \App\Http\Requests\EditAlkoholRequest;
use \App\Http\Requests\AddRecenzjaRequest;
use \App\Http\Requests\EditRecenzjaRequest;
use Illuminate\Http\JsonResponse;

use Auth;
use Redirect;

class IndexAdminController extends Controller {

	public function index()
	{
		return view('admin/index');
	}
	
	public function users()
	{
		$user = new Uzytkownik();
		
		$dane = ['usersList'=>$user->usersList(),"userCounter"=>1];
		return view('admin/usersPage',$dane);
	}
	
	public function reviews()
	{
		$recenzja = new Recenzja();
		$alkohol = new Alkohol();
		$dane = ['recenzjaList'=>$recenzja->recenzjaList(), "alkoholSelectList"=>$alkohol->alkoholSelectList(), "recCounter"=>1];
		return view('admin/reviews', $dane);
	}
	
	public function categories()
	{	
		$category = new Kategoria();
		
		$dane = ['categoryList'=>$category->catList(), "catCounter"=>1];
		
		return view('admin/categories', $dane);
		
	}
	
	public function alkohols()
	{
		$alkohol = new Alkohol();
		$category = new Kategoria();
		$dane = ['alkoholList'=>$alkohol->alkoholList() , "alkCounter"=>1, "kategoriaSelectOption"=>$category->categorySelectArray()];
		return view('admin/alkohols',$dane);
	}
	
	
	public function showCommentsByUser($id)
	{	
		$komentarz = new Komentarz();
		$dane = [
		"komList"=>$komentarz->showCommentsByUser($id),
		"komCounter"=>1
		];
		
		return view('admin/commentsPage',$dane);
	}
	
	public function delComment($id)
	{
		$komentarz = new Komentarz();
		$komentarz->deleteKom($id);
		return Redirect::back();
		
	}
	
	
	public function delUser($id)
	{
		$user = new Uzytkownik();
		try
		{
			$user->delUser($id);
			return Redirect::back();
		}catch(\Exception $e)
		{
			
		}
		
	}
	
	public function delRecenzja($id)
	{
		$recenzja = new Recenzja();
		try
		{
		$recenzja->delRecenzja($id);
			return Redirect::back();
		}
		catch(\Exception $e)
		{
			
		}
		
	}
	
	public function editRecenzja(EditRecenzjaRequest $request)
	{
		$recenzja = new Recenzja();
		try
		{
			$recenzja->editRecenzja();
		}
		catch(\Exception $e)
		{
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
	}
	
	public function addRecenzja(AddRecenzjaRequest $request)
	{
		$recenzja = new Recenzja();
		try
		{
			$recenzja->addRecenzja();
		}
		catch(\Exception $e)
		{
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
	}
	
	
	public function editAlkohol(EditAlkoholRequest $request)
	{
		$alkohol = new Alkohol();
		try
		{
			$alkohol->editAlkohol();
		}
		catch(\Exception $e)
		{
			return $e->getMessage();;
		}
	}
	
	public function delAlkohol($id)
	{
		$alkohol = new Alkohol();
		$alkohol->deleteAlkohol($id);
		return Redirect::back();
	}
	
	public function addAlkohol(AddAlkoholRequest $request)
	{
		$alkohol = new Alkohol();
		try
		{
			$alkohol->addNewAlkohol();
		}
		catch(\Exception $e)
		{
			echo $e->getMessage();
		}
		
		//return new JsonResponse(['msg'=>"gowno"], 422);
	}
	
	public function editCategory(EditCategoryRequest $request)
	{
		$category = new Kategoria();
		try
		{
			$category->editCategory();
		}
		catch(\Exception $e)
		{
			return new JsonResponse(['msg'=>$e->getMessage()], 422);	
		}
		
	}
	
	public function addCategory(AddCategoryRequest $request)
	{
		$category = new Kategoria();
		try{
			$category->addNewCat();
		}catch(\Exception $e){
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
		
	}
	
	public function delCategory($id){
		$category = new Kategoria();
		try{
			$category->delCat($id);
			return Redirect::back();
		}catch(\Exception $e){
			return Redirect::back()->with(['msg'=>$e->getMessage()]);
		}
	}

	

	
	
}