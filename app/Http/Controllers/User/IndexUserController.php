<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;


use App\Http\Requests\AddKomFormRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Models\Alkohol;
use App\Http\Models\Recenzja;
use App\Http\Models\Kategoria;

use \App\Http\Models\Komentarz;

use Auth;
use Redirect;

class IndexUserController extends Controller {


	public function comments()
	{
		$komentarz = new Komentarz();
		$dane = [
		"komList"=>$komentarz->showCommentsByUser(Auth::id()),
		"komCounter"=>1
		];
		
		return view('user/commentsPage',$dane);
	}
	
	
	public function delComment($id)
	{
		$komentarz = new Komentarz();
		$komentarz->deleteKomMy($id);
		return Redirect::back();
		
	}




	public function voteAlko()
	{
		try
		{
			$alkohol = new Alkohol();
			$alkohol->voteAlko();
			return $alkohol->getValueAVG();
		}
		catch(\Exception $e)
		{
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
	}

	public function addKomRec(AddKomFormRequest $request)
	{
			try
			{
				$kom = new Komentarz();
				$kom->addKomToRec();
			}
			catch(\Exception $e)
			{
				return new JsonResponse(['msg'=>$e->getMessage()], 422);
			}
	}

	public function addKom(AddKomFormRequest $request)
	{
			try
			{
				$kom = new Komentarz();
				$kom->addKomToAlko();
			}
			catch(\Exception $e)
			{
				return new JsonResponse(['msg'=>$e->getMessage()], 422);
			}
	}



}
