<?php namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Http\Models\Uzytkownik;


use App\Http\Requests\RegisterFormValidation;
use App\Http\Requests\LoginFormValidation;
use Illuminate\Http\JsonResponse;
use App\Http\Models\Alkohol;
use App\Http\Models\Recenzja;
use App\Http\Models\Kategoria;
use App\Http\Models\Komentarz;
use App\Http\Models\Glosy;

use Auth;
use Redirect;

class IndexGuestController extends Controller {

	public function index()
	{
		$komentarz = new Komentarz();
		$recenzja = new Recenzja();
		$alkohol = new Alkohol();
		$dane = ['alko'=>$alkohol->alkoholList(), "lastRec"=>$recenzja->lastRecenzja(5),
		'komLast'=>$komentarz->komLast(10)
		];
		return view('guest/index', $dane);
	}
	
	public function showRecenzjaList()
	{
		$recenzja = new Recenzja();
		$dane = ['recenzjaList'=>$recenzja->recenzjaList()];
		return view('guest/recenzjaList',$dane);
	}

	public function showRecenzja($id)
	{
		$recenzja = new Recenzja();
		$komentarz = new Komentarz();
		$dane = [
		'recenzjaInfo'=>$recenzja->recInfo($id),
		'komList'=>$komentarz->komListRec($id),
		];
		
		return view('guest/recenzjaPage',$dane);
	}
	
	public function showAlkohol($id)
	{
		$alkohol = new Alkohol();
		$komentarz = new Komentarz();
		$glos = new Glosy();
		$dane = ['alkoInfo'=>$alkohol->alkoInfo($id),
		"komList"=>$komentarz->komListAlko($id),
		"glosUR"=>$glos->myVote($id)
		];
		return view('guest/alkoholPage',$dane);
	}
	
	public function showCategory($id)
	{
		$recenzja = new Recenzja();
		$alkohol = new Alkohol();
		$category = new Kategoria();
		$komentarz = new Komentarz();
		$dane = ['alko'=>$alkohol->alkoholListFromCategory($id),
		"lastRec"=>$recenzja->lastRecenzja(5),
		"categoryName"=>$category->categoryName($id),
		'komLast'=>$komentarz->komLast(10)		
		];
		return view('guest/index', $dane);
	}
	
	
	public function logout(){
		if(Auth::check()){
			Auth::logout();
			return Redirect('/');
		}
	}
	
	
	public function activateAccount($code){
		$user = new Uzytkownik();
		$komentarz = new Komentarz();
		$recenzja = new Recenzja();
		$alkohol = new Alkohol();
		$dane = ['alko'=>$alkohol->alkoholList(), "lastRec"=>$recenzja->lastRecenzja(5),
		'komLast'=>$komentarz->komLast(10)
		];
		try{
			$dane['goodMsg'] = $user->activateUser($code);
			return view('guest/index', $dane);
		}catch(\Exception $e){
			$dane['badMsg'] = $e->getMessage();
			return view('guest/index', $dane);
		}
	}
	
	
	public function register(RegisterFormValidation $request){
		$user = new Uzytkownik();
		try{
			return $user->addNewUser();
		}catch(\Exception $e){
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
	}
	
	public function login(LoginFormValidation $request){
		$user = new Uzytkownik();
		try{
			$user->loginUser();
		}catch(\Exception $e){
			return new JsonResponse(['msg'=>$e->getMessage()], 422);
		}
	}

}
