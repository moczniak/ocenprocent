<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Validator;

use Illuminate\Support\Facades\Lang;

class AddKomFormRequest extends FormRequest {

	public function rules()
	{
		return [
			'komid' => 'required',
			'komVal' => 'required'
		];
	}
	
	public function messages()
	{
		return [
			'komid.required'=> 'Brak ID alkoholu',
			'komVal.required' => 'Musisz coś napisać'
		];
		
	}
	
	public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }
	
	/*
				$erro = $errors;

	*/

	
	
	public function response(array $errors)
    {
		if ($this->ajax() || $this->wantsJson())
		{
			foreach($errors as $key => $value)
			{
				return new JsonResponse(['msg'=>$errors[$key][0]], 422);
			}
			//return new JsonResponse($errors[$key][0], 422);
		}
		return $this->redirector->to($this->getRedirectUrl())
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag);
    }
	
	

}
