<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Validator;

use Illuminate\Support\Facades\Lang;

class AddAlkoholRequest extends FormRequest {

	public function rules()
	{
		return [
			'alkName' => 'required',
			'alkDesc' => 'required',
			'alkKategoria' => 'required'
		];
	}
	
	public function messages()
	{
		return [
			'alkName.required'=> 'Nazwa Alkoholu wymagana',
			'alkDesc.required' => 'Opis Alkoholu musi być',
			'alkKategoria' => 'Kategoria Wymagana'
		];
		
	}
	
	public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }
	
	/*
				$erro = $errors;

	*/

	
	
	public function response(array $errors)
    {
		if ($this->ajax() || $this->wantsJson())
		{
			foreach($errors as $key => $value)
			{
				return new JsonResponse($errors[$key][0], 422);
			}
			//return new JsonResponse($errors[$key][0], 422);
		}
		return $this->redirector->to($this->getRedirectUrl())
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag);
    }
	
	

}
