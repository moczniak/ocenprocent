<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Validator;

use Illuminate\Support\Facades\Lang;

class EditAlkoholRequest extends FormRequest {

	public function rules()
	{
		return [
			'alkidE' => 'required',
			'alkNameE' => 'required',
			'alkDescE' => 'required',
			'alkKategoriaE' => 'required'
		];
	}
	
	public function messages()
	{
		return [
			'alkidE.required' => 'ID wymagane!',
			'alkNameE.required'=> 'Nazwa Alkoholu wymagana',
			'alkDescE.required' => 'Opis Alkoholu musi by�',
			'alkKategoriaE' => 'Kategoria Wymagana'
		];
		
	}
	
	public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }
	
	
	public function response(array $errors)
    {
		if ($this->ajax() || $this->wantsJson())
		{
			foreach($errors as $key => $value)
			{
				return new JsonResponse($errors[$key][0], 422);
			}
			//return new JsonResponse($errors[$key][0], 422);
		}
		return $this->redirector->to($this->getRedirectUrl())
                                        ->withInput($this->except($this->dontFlash))
                                        ->withErrors($errors, $this->errorBag);
    }
	
	

}
