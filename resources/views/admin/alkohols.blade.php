@include('admin.pageHeader')

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif

	<div class="container">
	<div class="row">
	
	<button class="btn btn-primary" data-toggle="modal" data-target="#dodajModal">Dodaj</button>
	<div style="width:100%; height:20px;"></div>
	
	<!--DODAJ Alkohol Modal -->
<div class="modal fade" id="dodajModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodawarka Procentów</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'addAlkoholForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError"></span></center>
			
				<div class="progress progress-striped active" id="progressbar1" style="display:none;">
					<div class="progress-bar progress-bar-success" id="progressbar1Width" style="width: 0%"></div>
				</div>
			
			  <div class="form-group">
				<input type="text" class="form-control" id="alkName" name="alkName" placeholder="Podaj Nazwe Alkoholu">
				<br>
				<textarea class="form-control" rows="4" name="alkDesc" placeholder="Opis Alkoholu"></textarea>
				<br>
				{!! Form::select('alkKategoria', $kategoriaSelectOption,null, array("class"=>"form-control")) !!}
				<br>
					{!! Form::label('image', 'Obrazek Alkoholu', array( 'id'=>'artFile')) !!}
					<br>
					{!! Form::file('image') !!}
			  </div>

		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="addCategory" form="addAlkoholForm">Dodaj</button>
		

		
      </div>
    </div>
  </div>
</div>
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			
		
			
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Nazwa</th>
				  <th>Kategoria</th>
				  <th>Recenzja</th>
				  <th>Średnia Ocena</th>
				  <th>Akcja</th>
				</tr>
			  </thead>
			  <tbody>
				@foreach($alkoholList as $at)
				<tr>
				  <td>{!! $alkCounter++ !!}</td>
				  <td>{!! $at->nazwa !!} <a href="#" class="alkEdit" data-alkid="{!!$at->alkohol_id !!}" data-alkName="{!!$at->nazwa !!}" data-alkDesc="{!!$at->text !!}" data-alkKat="{!! $at->kategoria_id !!}" data-alkIMG="{!! $at->img !!}" data-toggle="modal" data-target="#editAlkohol">(Edytuj)</a></td>
				  <td>{!! $at->kategoria->nazwa !!}</td>
				  <td>
					@if(!empty($at->recenzja))
						<a href="#" class="recEdit" data-recid="{!! $at->recenzja->recenzja_id !!}" data-text="{!! $at->recenzja->text !!}" data-toggle="modal" data-target="#editRecenzja">(edytuj)</a>
					@else
						<a href="#" class="adRec" data-alkoid="{!! $at->alkohol_id !!}" data-toggle="modal" data-target="#dodajRecModal">(dodaj)</a>
					@endif
				  
				  </td>
					@if($at->glosyLiczba > 0)
							<td>{!! $at->glosySuma / $at->glosyLiczba !!}</td>
					@else
							<td>0</td>
					@endif
				  <th><a href="{!! url::to('admin/delAlkohol', array('id'=>$at->alkohol_id)) !!}">Skasuj</a></th>
				</tr>
				
				@endforeach
			  </tbody>
			</table>
			
		</div>
	</div>
	</div>
	
<script>
$('.alkEdit').click(function(e){
	e.preventDefault();
	$('.alkidE').val($(this).attr('data-alkid'));
	$('#alkNameE').val($(this).attr('data-alkName'));
	$('#alkDescE').val($(this).attr('data-alkDesc'));
	$('.alkSelectE').val($(this).attr('data-alkKat'));
	var link = 'http://'+location.host+'/';
	if($(this).attr('data-alkIMG') != ""){
		$('.alkIMGE').show();
		$('.alkIMGE').attr('src',link+$(this).attr('data-alkIMG'));
	}else $('.alkIMGE').hide();
});

</script>	
	
	<!--Edytuj Alkohol Modal -->
<div class="modal fade" id="editAlkohol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Edycja Alkoholu</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'editAlkoholForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError1"></span></center>
			
				<div class="progress progress-striped active" id="progressbar2" style="display:none;">
					<div class="progress-bar progress-bar-success" id="progressbar1Width2" style="width: 0%"></div>
				</div>
				<input type="hidden" class="alkidE" name="alkidE">
			  <div class="form-group">
				<input type="text" class="form-control" id="alkNameE" name="alkNameE" placeholder="Podaj Nazwe Alkoholu">
				<br>
				<textarea class="form-control" rows="4" id="alkDescE" name="alkDescE" placeholder="Opis Alkoholu"></textarea>
				<br>
				{!! Form::select('alkKategoriaE', $kategoriaSelectOption,null, array("class"=>"form-control alkSelectE")) !!}
				<br>
					{!! Form::label('image', 'Jeśli chcesz zmienić wybierz nowy', array( 'id'=>'artFileE')) !!}
					<br>
					{!! Form::file('image') !!}
			  </div>
				<i>Obecny Obrazek</i>
				
					<img src="" class="img-responsive alkIMGE">
				
				
		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="addCategory" form="editAlkoholForm">Zapisz</button>
		

		
      </div>
    </div>
  </div>
</div>
			
	

{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}


<script>
$('#artFileE').on('change', handleFileSelect);
$('#editAlkoholForm').on('submit',{ formerror: '.logError1', actionUrl : "{!! URL::to('admin/editAlkohol') !!}", progressbar: '#progressbar2', progressbarWidth: '#progressbar1Width2' }, handleFormSubmit);

</script>
		
		
<script>
$('#artFile').on('change', handleFileSelect);
$('#addAlkoholForm').on('submit',{ formerror: '.logError', actionUrl : "{!! URL::to('admin/addAlkohol') !!}", progressbar: '#progressbar1', progressbarWidth: '#progressbar1Width' }, handleFormSubmit);

</script>



<script>
//data-alkoid
$('.adRec').click(function(e){
	$('.alkidA').val($(this).attr('data-alkoid'));
});
</script>



	<!--DODAJ Alkohol Rec Modal -->
<div class="modal fade" id="dodajRecModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodawarka Recenzji</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'addRecenzjaForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError"></span></center>
						
			  <div class="form-group">
				<input type="hidden" name="alkidA" class="alkidA">
				<textarea class="form-control" rows="4" id="recTxt" name="recTxt" placeholder="Recenzja"></textarea>
			  </div>

		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="addRecenzja">Dodaj</button>
		
<script>
	$("#addRecenzja").click(function(event){
				$.post("{!! URL::to('/admin/addRecenzja') !!}",
					{ 	_token : $('#addRecenzjaForm input[name=_token]').val(),
						recTxt : $('#addRecenzjaForm #recTxt').val(),
						alkid : $('#addRecenzjaForm .alkidA').val()
					},		 
					function(data){
						if(data == ""){
							location.reload();
						}
					}
				).error(function(request, status, error){///
					$('.logError').html(firstJsonResponse(request.responseText));
				});
	});

</script>
		
      </div>
    </div>
  </div>
</div>





	<!--Edytuj Alkohol Modal -->
<div class="modal fade" id="editRecenzja" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Edycja Recenzji</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'editRecenzjaForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError1"></span></center>
			
				<input type="hidden" class="recid" name="recid">
			  <div class="form-group">
				<textarea class="form-control" rows="6" id="recTxtE" name="recTxtE" placeholder="Opis Alkoholu"></textarea>
			  </div>
		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="editRec">Zapisz</button>
		

		
      </div>
    </div>
  </div>
</div>

	<script>
$('.recEdit').click(function(e){
	e.preventDefault();//
	$('.recid').val($(this).attr('data-recid'));
	$('#recTxtE').html($(this).attr('data-text'));
});


$('#editRec').click(function(e){
	e.preventDefault();//
	$.post("{!! URL::to('/admin/editRecenzja') !!}",
		{ 	_token : $('#editRecenzjaForm input[name=_token]').val(),
						recTxt : $('#editRecenzjaForm #recTxtE').val(),
						recid : $('#editRecenzjaForm .recid').val()
		},		 
			function(data){
				if(data == ""){
					location.reload();
				}
			}
		).error(function(request, status, error){///
			$('.logError1').html(firstJsonResponse(request.responseText));
	});
});

</script>


</body>