@include('admin.pageHeader')

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif

	<div class="container">
	<div class="row">
	
	<button class="btn btn-primary" data-toggle="modal" data-target="#dodajModal">Dodaj</button>
	<div style="width:100%; height:20px;"></div>
	
	<!--DODAJ Modal -->
<div class="modal fade" id="dodajModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodawarka kategorii</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'addCategoryForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError"></span></center>
			  <div class="form-group">
				<label for="catName">Nazwa</label>
				<input type="email" class="form-control" id="catName" name="catName" placeholder="Podaj Nazwe Kategorii">
			  </div>

			
		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="button" class="btn btn-primary" id="addCategory">Dodaj</button>
		
		<script>
			$("#addCategory").click(function(event){
				$.post("{!! URL::to('/admin/addCategory') !!}",
					{ _token : $('#addCategoryForm input[name=_token]').val(),
					catName : $('#addCategoryForm input[name=catName]').val()
					},		 
					function(data){
						if(data == ""){
							location.reload();
						}
					}
				).error(function(request, status, error){///
					$('.logError').html(firstJsonResponse(request.responseText));
				});
			});
		</script>
		
      </div>
    </div>
  </div>
</div>
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			
		
			
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Nazwa</th>
				  <th>Alkohole</th>
				  <th>Akcja</th>
				</tr>
			  </thead>
			  <tbody>
				@foreach($categoryList as $ct)
				<tr>
				  <td>{!! $catCounter++ !!}</td>
				  <td>{!! $ct->nazwa !!} <a href="#" class="catEditLink" data-toggle="modal" data-target="#catEdit" data-catid="{!! $ct->kategoria_id !!}" data-catName="{!! $ct->nazwa !!}">(Edytuj)</a></td>
				  <td>{!! count($ct->alkoholCount)  !!}</td>
				  <th><a href="{!! url::to('admin/catDel', array($ct->kategoria_id))!!}">Skasuj</a></th>
				</tr>
				
				@endforeach
			  </tbody>
			</table>
			
		</div>
	</div>
	</div>
	
<script>
$('.catEditLink').click(function(e){
	e.preventDefault();
	$('.oldCatName').html($(this).attr('data-catName'));
	$('.catid').val($(this).attr('data-catid'));
});

</script>	
	
	<!--CATEDIT Modal -->
<div class="modal fade" id="catEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Edycja Kategorii</h4>
      </div>
      <div class="modal-body">
		<center><span style="color:#D8230F;font-weight:bold;" class="logError2"></span></center>
        <p><i class="oldCatName">Stara Nazwa</i></p>
		{!! Form::open(array('role'=>'form', 'id'=>'editCategoryForm')) !!}
		<input type="hidden" class="catid" name="catid">
		<input type="text"class="form-control catName" name="catName" placeholder="NowaNazwa">
		{!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="button" class="btn btn-primary catEditSave">Zapisz</button>
      </div>
    </div>
  </div>
</div>
	
<script>
			$(".catEditSave").click(function(event){
				$.post("{!! URL::to('/admin/editCategory') !!}",
					{ _token : $('#editCategoryForm input[name=_token]').val(),
					catName : $('#editCategoryForm input[name=catName]').val(),
					catId : $('#editCategoryForm input[name=catid]').val()
					},		 
					function(data){
						if(data == ""){
							location.reload();
						}
					}
				).error(function(request, status, error){///
					$('.logError2').html(firstJsonResponse(request.responseText));
				});
			});

</script>			
	

{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>