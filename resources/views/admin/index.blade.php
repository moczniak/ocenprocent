@include('admin.pageHeader')

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif

	<div class="container">
	<div class="row">
	
	<button class="btn btn-primary" data-toggle="modal" data-target="#dodajModal">Dodaj</button>
	<div style="width:100%; height:20px;"></div>
	
	<!--DODAJ Modal -->
<div class="modal fade" id="dodajModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodawarka procentów</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form')) !!}
			
			  <div class="form-group">
				<label for="exampleInputEmail1">Nazwa</label>
				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Podaj Nazwe Alkoholu">
			  </div>
			  <div class="form-group">
				<label for="exampleInputPassword1">Kategoria</label>
				<select class="form-control" id="exampleInputPassword1">
				  <option>Opcja 1</option>
				  <option>Opcja 2</option>
				  <option>Opcja 3</option>
				  <option>Opcja 4</option>
				  <option>Opcja 5</option>
				</select>
			  </div>
			<div class="form-group">
				<label for="exampleInputFile">Tekst</label>
				<textarea class="form-control" id="exampleInputFile" rows="4">Textarea z 4 rzędami</textarea>

			  </div>
			  <div class="form-group">
				<label for="exampleInputFile">Dołącz obrazek</label>
				<input type="file" id="exampleInputFile">
			  </div>
			
		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="button" class="btn btn-primary">Dodaj</button>
      </div>
    </div>
  </div>
</div>
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			
			
			
			
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Imię</th>
				  <th>Nazwisko</th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
				  <td>1</td>
				  <td>Jan</td>
				  <td>Kowalski</td>
				</tr>
				<tr>
				  <td>2</td>
				  <td>Jakub</td>
				  <td>Nowak</td>
				</tr>
			  </tbody>
			</table>
			
		</div>
	</div>
	</div>
	
	
	
	
	

{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>