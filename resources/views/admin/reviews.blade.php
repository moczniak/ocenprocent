@include('admin.pageHeader')

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif

	<div class="container">
	<div class="row">
	
	<button class="btn btn-primary" data-toggle="modal" data-target="#dodajModal">Dodaj</button>
	<div style="width:100%; height:20px;"></div>
	
	<!--DODAJ Alkohol Modal -->
<div class="modal fade" id="dodajModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodawarka Recenzji</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'addRecenzjaForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError"></span></center>
						
			  <div class="form-group">
				<textarea class="form-control" rows="4" id="recTxt" name="recTxt" placeholder="Recenzja"></textarea>
				<br>
				{!! Form::select('alkid', $alkoholSelectList,null, array("class"=>"form-control alkid")) !!}
			  </div>

		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="addRecenzja">Dodaj</button>
		
<script>
	$("#addRecenzja").click(function(event){
				$.post("{!! URL::to('/admin/addRecenzja') !!}",
					{ 	_token : $('#addRecenzjaForm input[name=_token]').val(),
						recTxt : $('#addRecenzjaForm #recTxt').val(),
						alkid : $('#addRecenzjaForm .alkid').val()
					},		 
					function(data){
						if(data == ""){
							location.reload();
						}
					}
				).error(function(request, status, error){///
					$('.logError').html(firstJsonResponse(request.responseText));
				});
	});

</script>
		
      </div>
    </div>
  </div>
</div>
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			
		
			
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Alkohol</th>
				  <th>Skrót</th>
				  <th>Akcja</th>
				</tr>
			  </thead>
			  <tbody>
				@foreach($recenzjaList as $rc)
				<tr>
				  <td>{!! $recCounter++ !!}</td>
				  <td>{!! $rc->alkohol->nazwa !!} <a href="#" class="recEdit" data-recid="{!! $rc->recenzja_id !!}" data-text="{!! $rc->text !!}" data-toggle="modal" data-target="#editRecenzja">(Edytuj)</a></td>
				<td>{!! str_limit($rc->text,100,'...') !!}</td>
				  <th><a href="{!! url::to('admin/delRecenzja', array('id'=>$rc->recenzja_id)) !!}">Skasuj</a></th>
				</tr>
				
				@endforeach
			  </tbody>
			</table>
			
		</div>
	</div>
	</div>
	

	
	
	<!--Edytuj Alkohol Modal -->
<div class="modal fade" id="editRecenzja" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zamknij</span></button>
        <h4 class="modal-title" id="myModalLabel">Edycja Recenzji</h4>
      </div>
      <div class="modal-body">
        
		{!! Form::open(array('role'=>'form', 'id'=>'editRecenzjaForm')) !!}
			<center><span style="color:#D8230F;font-weight:bold;" class="logError1"></span></center>
			
				<input type="hidden" class="recid" name="recid">
			  <div class="form-group">
				<textarea class="form-control" rows="6" id="recTxtE" name="recTxtE" placeholder="Opis Alkoholu"></textarea>
			  </div>
		{!! Form::close() !!}
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        <button type="submit" class="btn btn-primary" id="editRec">Zapisz</button>
		

		
      </div>
    </div>
  </div>
</div>
					
	<script>
$('.recEdit').click(function(e){
	e.preventDefault();//
	$('.recid').val($(this).attr('data-recid'));
	$('#recTxtE').html($(this).attr('data-text'));
});


$('#editRec').click(function(e){
	e.preventDefault();//
	$.post("{!! URL::to('/admin/editRecenzja') !!}",
		{ 	_token : $('#editRecenzjaForm input[name=_token]').val(),
						recTxt : $('#editRecenzjaForm #recTxtE').val(),
						recid : $('#editRecenzjaForm .recid').val()
		},		 
			function(data){
				if(data == ""){
					location.reload();
				}
			}
		).error(function(request, status, error){///
			$('.logError1').html(firstJsonResponse(request.responseText));
	});
});

</script>











{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>