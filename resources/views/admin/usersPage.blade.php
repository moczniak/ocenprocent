@include('admin.pageHeader')

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif

	<div class="container">
	<div class="row">
	
	<div style="width:100%; height:20px;"></div>
	

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			
		
			
			<table class="table">
			  <thead>
				<tr>
				  <th>#</th>
				  <th>Nazwa</th>
				  <th>Email</th>
				  <th>Komentarze</th>
				  <th>Akcja</th>
				</tr>
			  </thead>
			  <tbody>
				@foreach($usersList as $us)
				<tr>
					<td>{!! $userCounter++ !!}</td>
					<td>{!! $us->username !!}</td>
					<td>{!! $us->email !!}</td>
					<td><a href="{!! url::to('admin/showComments',array('id'=>$us->uuid)) !!}">Komentarze</a></td>
					<td><a href="{!! url::to('admin/delUser',array('id'=>$us->uuid)) !!}">Skasuj</a></td>
				</tr>
				
				@endforeach
			  </tbody>
			</table>
			
		</div>
	</div>
	</div>
	

			
	

{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}



	<script>


$('#editRec').click(function(e){
	e.preventDefault();//
	$.post("{!! URL::to('/admin/editRecenzja') !!}",
		{ 	_token : $('#editRecenzjaForm input[name=_token]').val(),
						recTxt : $('#editRecenzjaForm #recTxtE').val(),
						recid : $('#editRecenzjaForm .recid').val()
		},		 
			function(data){
				if(data == ""){
					location.reload();
				}
			}
		).error(function(request, status, error){///
			$('.logError1').html(firstJsonResponse(request.responseText));
	});
});

</script>


</body>