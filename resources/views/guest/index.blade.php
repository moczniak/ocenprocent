@include('guest.pageHeader', ['login'=>$login, 'admin'=>$admin, 'category'=>$category])

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif


	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px;">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<h3><center>Najnowsze Recenzje</center></h3>
				
				@foreach($lastRec as $lr)
				<div>
				<a href="{!! url::to('showRec', array('id'=> $lr->recenzja_id)) !!}">{!! $lr->alkohol->nazwa !!}</a> - {!! str_limit($lr->text, 100,'...') !!}
				</div>
				@endforeach
			
			
			
				<h3><center>Najnowsze Komentarze</center></h3>
				
				@foreach($komLast as $km)
				{!! $km->uzytkownik->username !!} - {!! str_limit($km->text,  100, '...') !!} <br><br>
				@endforeach
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<h1><center>
				@if(isset($categoryName))
				{!! $categoryName !!}
				@else
				Najlepsze
				@endif
				
				
				</center></h1>
					@if(!empty($alko))
					@foreach($alko as $st)
					<div class="row">
					<h4><center><a href="{!! url::to('showAlk', array('id'=>$st->alkohol_id)) !!}">{!! $st->nazwa !!}</a></center></h4>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-lg-offset-2 col-lg-offset-md col-sm-offset-2">
					{!!	HTML::image($st->img, '$st->nazwa', array('class' => 'img-responsive')) !!}
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						
						<p>
						{!! $st->text!!}
						
						</p>
						<h5><B>Ocena:
						@if($st->glosyLiczba > 0)
						{!! $st->glosySuma / $st->glosyLiczba !!}
						@else
							0.0
						@endif
						</b></h5>
						
					</div>
					</div>
					@endforeach
					@else
						<p>Brak Procentów</p>
					@endif
			</div>
		
		</div>	
	</div>
	</div>


{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>