@include('guest.pageHeader', ['login'=>$login, 'admin'=>$admin, 'category'=>$category])

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif


	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px; padding-bottom:30px;">
			<h2><center>{!! $alkoInfo[0]->nazwa !!}</center></h2>
			<div class="col-lg-3 col-md-3 col-sm-3 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-12">
				{!!	HTML::image($alkoInfo[0]->img, '$alkoInfo[0]->nazwa', array('class' => 'img-responsive')) !!}
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			
			<p>
			
{!! $alkoInfo[0]->text !!}
			</p>
			<h5><B>Ocena: 
			@if($alkoInfo[0]->glosyLiczba > 0)
				<span class="voteAVG">{!! $alkoInfo[0]->glosySuma / $alkoInfo[0]->glosyLiczba !!}</span>
			@else
				<span class="voteAVG">0</span>
			@endif

			</b></h5>
			
			@if($login)
			<h5><b>Oceń ten procent</b></h5>
			<b><center style="color:red;" class="votError"></center></b>
			{!! Form::open(array("role"=>"form", "id"=>"voteAlk")) !!}
			<input type="hidden" name="komid" value="{!! $alkoInfo[0]->alkohol_id !!}">
			{!! Form::close() !!}
			
			
			<?php
			$g = 1;
			for($h =0; $h <$glosUR; ++$h){?>
				<i class="fa fa-beer vots" id="<?=$g;?>" style="font-size:5em; cursor:pointer; opacity:1;"></i>	
			<?
			++$g; 
			}
			for($h = 0; $h < (5-$glosUR);$h++){?>
				<i class="fa fa-beer vots" id="<?=$g;?>" style="font-size:5em; cursor:pointer; opacity:0.3;"></i>	
			<?
				++$g;
			}?>
			
			
			<script>
			$('.vots').click(function(){
				var click = this.id;
				$('.vots').each(function(){
					if(this.id <= click){
						$(this).css({'opacity':1});
						//$(this).attr("src", "assets/images/inne/star_yes.png");
					}else{
						$(this).css({'opacity':0.3});
						//$(this).attr("src", "assets/images/inne/star_no.png");
					}
				});
						$.post("{!! URL::to('/user/voteAlk') !!}",
							{ _token : $('#voteAlk input[name=_token]').val(),
							komid: $('#komForm input[name=komid]').val(),
							voteVal : parseInt(click)
							},		 
							function(data){
								$('.voteAVG').html(data);
							}
						).error(function(request, status, error){
							console.log(request);
							$('.votError').html(firstJsonResponse(request.responseText));
						});
					
			
			});
			
			</script>
			@else
				<h5><b>Zaloguj sie aby zagłosować</b></h5>
			
			@endif
			
			
			
			</div>
			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3><center>Komentarze</center></h3>
					
					@if($login)
					{!! Form::open(array("role"=>"form", "id"=>"komForm")) !!}
						<b><center style="color:red;" class="komError"></center></b>
						<input type="hidden" name="komid" value="{!! $alkoInfo[0]->alkohol_id !!}">
					<textarea class="form-control komVal" name="komVal" rows="3" placeholder="zostaw komentarz"></textarea>
					<button class="btn btn-default btn-lg kombtn">Dodaj</button>
					{!! Form::close() !!}
					
					<script>
					$(".kombtn").click(function(event){
						event.preventDefault();
						$.post("{!! URL::to('/user/addKom') !!}",
							{ _token : $('#komForm input[name=_token]').val(),
							komid: $('#komForm input[name=komid]').val(),
							komVal : $('.komVal').val()
							},		 
							function(data){
								if(data == ""){
									location.reload();
								}
							}
						).error(function(request, status, error){
							console.log(request);
							$('.komError').html(firstJsonResponse(request.responseText));
						});
					});
				</script>
					
					@else
						<p><center><i>Musisz być zalogowany by komentować</i></center></p>
					@endif
					<div style="width:100%; height:30px;"></div>
					<ul style="list-style:none; padding:0; margin:0;">
					@foreach($komList as $km)
						<li><b>{!! $km->uzytkownik->username !!}</b> - {!! $km->text !!} <li>
					@endforeach
					</ul>
					
					
				</div>
			</div>
			
			
			
			
		</div>	
	</div>
	</div>


{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>