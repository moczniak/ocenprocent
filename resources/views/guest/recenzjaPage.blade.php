@include('guest.pageHeader', ['login'=>$login, 'admin'=>$admin, 'category'=>$category])

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif


	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px; padding-bottom:30px;">
			<h2><center>{!! $recenzjaInfo->alkohol->nazwa !!}</center></h2>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				{!!	HTML::image($recenzjaInfo->alkohol->img, '$recenzjaInfo->alkohol->nazwa', array('class' => 'img-responsive')) !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			
			<p>
			
				{!! $recenzjaInfo->text !!}
			</p>
			
			
			</div>
			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3><center>Komentarze</center></h3>
					
					@if($login)
					{!! Form::open(array("role"=>"form", "id"=>"komForm")) !!}
						<b><center style="color:red;" class="komError"></center></b>
						<input type="hidden" name="komid" value="{!! $recenzjaInfo->recenzja_id !!}">
					<textarea class="form-control komVal" name="komVal" rows="3" placeholder="zostaw komentarz"></textarea>
					<button class="btn btn-default btn-lg kombtn">Dodaj</button>
					{!! Form::close() !!}
					
					<script>
					$(".kombtn").click(function(event){
						event.preventDefault();
						$.post("{!! URL::to('/user/addKomRec') !!}",
							{ _token : $('#komForm input[name=_token]').val(),
							komid: $('#komForm input[name=komid]').val(),
							komVal : $('.komVal').val()
							},		 
							function(data){
								if(data == ""){
									location.reload();
								}
							}
						).error(function(request, status, error){
							console.log(request);
							$('.komError').html(firstJsonResponse(request.responseText));
						});
					});
				</script>
					
					@else
						<p><center><i>Musisz być zalogowany by komentować</i></center></p>
					@endif
					<div style="width:100%; height:30px;"></div>
					<ul style="list-style:none; padding:0; margin:0;">
					@foreach($komList as $km)
						<li><b>{!! $km->uzytkownik->username !!}</b> - {!! $km->text !!} <li>
					@endforeach
					</ul>
					
					
				</div>
			</div>
			
			
			
			
		</div>	
	</div>
	</div>


{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>