@include('guest.pageHeader', ['login'=>$login, 'admin'=>$admin, 'category'=>$category])

	@if(isset($goodMsg))
	<div class="alert alert-success alert-dismissable col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $goodMsg !!}
	</div>
	@endif
	@if(isset($badMsg))
	<div class="alert alert-danger alert-dismissable  col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  {!! $badMsg !!}
	</div>
	@endif


	<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:white; border-radius:5px; padding-bottom:30px;">
					<h1><center>Recenzje</center></h1>
			<table class="table">
				<thead>
					<th>Trunek</th>
					<th>Skrót</th>
				</thead>
					@foreach($recenzjaList as $rc)
					<tr>
					<td><a href="{!! url::to('showRec', array('id'=>$rc->recenzja_id)) !!}">{!! $rc->alkohol->nazwa !!}</a></td>
					<td>{!! str_limit($rc->text,  100, '...') !!}</td>
					</tr>
					@endforeach
			</table>

			
			
			
			
		</div>	
	</div>
	</div>


{!! HTML::script('resources/assets/js/ajaxFileForm.js') !!}

</body>