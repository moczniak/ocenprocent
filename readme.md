phpmyadmin:
http://s81.vdl.pl/phpmyadmin/

login: moczniak_student
haslo: moczniak_grupa1

FTP:
host: s81.vdl.pl
login: student@moczniak.org
haslo: grupa1


Jesli chcesz sie tu bawic to sie baw, ale jak cos zmienisz to daj znac^^
jesli chcesz sie bawic tym projektem na swoim lokalnym serwerze, to musisz miec php 5.4> z zainstalowanym composerem.

wchodzisz na bitbucketa mego: https://bitbucket.org/moczniak/ocenprocent
klonujesz go na kompa swego, wrzucasz wypakowane pliki na swoj serwer wbijasz w niego w konsoli i wklepujesz
composer update

jesli masz composera zainstalowanego globalnie^^ jesli lokalnie to wklepujesz
php composer.phar update

powinny ci sie posciagac paczuszki i projekt gotowy, nie zapomnij bazy sobie wrzucic na swojego mysql'a i zmienic danych w config/database.php w sekcji mysql



## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
